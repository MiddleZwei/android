package com.ihalych.myapplication.githubapi

import com.google.gson.annotations.SerializedName

data class UserResult(
    val result: List<User>
)

data class User(
    @SerializedName("login") val login: String,
    @SerializedName("id") val id: Long,
    @SerializedName("url") val url: String,
    @SerializedName("html_url") val html_url: String,
    @SerializedName("followers_url") val followers_url: String,
    @SerializedName("following_url") val following_url: String,
    @SerializedName("starred_url") val starred_url: String,
    @SerializedName("gists_url") val gists_url: String,
    @SerializedName("type") val type: String
)

data class Person(
    val login: String
) {
    override fun toString(): String {
        return "$login FOUND"
    }
}