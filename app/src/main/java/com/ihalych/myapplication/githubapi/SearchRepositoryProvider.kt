package com.ihalych.myapplication.githubapi

class SearchRepositoryProvider() {

    object SearchRepositoryProvider {
        fun provideSearchRepository(): SearchRepository {
            return SearchRepository(GitHubApiService.create())
        }
    }
}

