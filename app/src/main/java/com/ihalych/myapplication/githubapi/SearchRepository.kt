package com.ihalych.myapplication.githubapi

import io.reactivex.Observable
import io.reactivex.ObservableSource


class SearchRepository(val api: GitHubApiService) {

    fun getAllUsers(): Observable<Person> {
        return api.getUsers()
            .flatMap { listUserResult -> Observable.fromIterable(listUserResult) }
//            .flatMap { userResult -> Observable.fromIterable(userResult.result) }
            .map { user -> Person(user.login) }
    }

    fun getUser(login: String): Observable<User> { //person?
        return api.getUser(login)
    }


}

