package com.ihalych.myapplication.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ihalych.myapplication.MyApplication
import com.ihalych.myapplication.R
import com.ihalych.myapplication.R.id.recyclerView
import com.ihalych.myapplication.githubapi.GitHubApiService
import com.ihalych.myapplication.githubapi.User
import com.ihalych.myapplication.ui.recyclerview.UserRecyclerAdapter
import com.ihalych.myapplication.ui.vm.ItemViewModel
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ListFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null

    private var recyclerView: RecyclerView? = null
    private var viewModel: ItemViewModel? = null
    private var adapter: UserRecyclerAdapter? = null
    private var persons: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //managing dependencies
        viewModel = (this.activity?.application as MyApplication).getMainPresenter()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        //init everything
        recyclerView = view?.findViewById(R.id.recyclerView)
        //persons = mutableListOf()
        adapter = UserRecyclerAdapter(persons)
        recyclerView?.layoutManager = LinearLayoutManager(activity)
        recyclerView?.adapter = adapter

        // !! <- need to think how to handle this case tho
        viewModel?.loadDataFromApi()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ person ->
                persons?.add(person.login)
            }, {e ->
                e.printStackTrace()
            }, {
                adapter?.notifyDataSetChanged()
            })

        return view
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(list: List<User>)
    }

    companion object {
        fun newInstance() = ListFragment()
    }
}
