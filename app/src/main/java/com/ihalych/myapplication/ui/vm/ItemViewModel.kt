package com.ihalych.myapplication.ui.vm

import android.arch.lifecycle.ViewModel
import com.ihalych.myapplication.githubapi.Person
import com.ihalych.myapplication.githubapi.SearchRepository
import com.ihalych.myapplication.githubapi.User
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable


// PURPOSE: maintain state of the view,
//          manipulate model as the result of actions on the view,
//          trigger events in the view


//1. manage repository: get observable data [X], return a list
//2. manage activities in the view:

class ItemViewModel(private val repository: SearchRepository) : ViewModel(){
    fun loadDataFromApi(): Observable<Person> = repository.getAllUsers()
}