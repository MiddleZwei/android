package com.ihalych.myapplication.ui.recyclerview

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ihalych.myapplication.R
import com.ihalych.myapplication.githubapi.Person
import com.ihalych.myapplication.githubapi.User
import kotlinx.android.synthetic.main.list_view_holder.view.*

class UserRecyclerAdapter(
    private val persons: List<String>
) : RecyclerView.Adapter<UserRecyclerAdapter.UsersHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.list_view_holder, parent, false)
        return UsersHolder(inflatedView)
    }

    override fun getItemCount(): Int = persons.size

    override fun onBindViewHolder(holder: UsersHolder, position: Int) {
        val person = persons[position]
        holder.bindData(person)
    }


    class UsersHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var person: String? = null

        init {
            v.setOnClickListener { this }
        }

        override fun onClick(v: View?) {
            Log.d("RecyclerView", "CLICK!")
        }

        fun bindData(person: String){
            this.person = person
            view.itemTitle.text = person
        }

    }

}