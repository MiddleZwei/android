package com.ihalych.myapplication

import android.app.Application
import com.ihalych.myapplication.githubapi.SearchRepository
import com.ihalych.myapplication.githubapi.SearchRepositoryProvider
import com.ihalych.myapplication.ui.vm.ItemViewModel
import retrofit2.Retrofit

class MyApplication : Application() {

    fun getMainPresenter(): ItemViewModel = ItemViewModel(getRepository())

    private fun getRepository(): SearchRepository = SearchRepositoryProvider.SearchRepositoryProvider.provideSearchRepository()

}
