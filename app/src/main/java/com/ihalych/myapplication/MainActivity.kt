package com.ihalych.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.ihalych.myapplication.githubapi.User
import com.ihalych.myapplication.ui.ListFragment

class MainActivity : AppCompatActivity(),
    ListFragment.OnFragmentInteractionListener {

    private val TAG : String = "MainActivity"
    private val fragment: ListFragment =
        ListFragment.newInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager
            .beginTransaction()
            .add(R.id.list_fragment, fragment)
            .commit()
    }

    override fun onFragmentInteraction(list: List<User>) {
        Log.d("Interacting with fragment", "clickclickclick")
    }
}
